package view

import (
  "html/template"
  "net/http"
  "path/filepath"
)

var v View
var LayoutDir string = "view/layout"

type View struct {
  Template *template.Template
  Layout   string
}

type ViewData struct {
  Data    interface{}
}

func NewView(layout string, files ...string) *View {
  files = append(files, layoutFiles()...)
  t, err := template.New(layout).ParseFiles(files...)
  if err != nil {
    panic(err)
  }

  return &View{
    Template: t,
    Layout:   layout,
  }
}

func layoutFiles() []string {
  files, err := filepath.Glob(LayoutDir + "/*.gohtml")
  if err != nil {
    panic(err)
  }
  return files
}

func (v *View) Render(w http.ResponseWriter, data ...interface{}) error {
  vd := ViewData{
    Data: data,
  }
  return v.Template.ExecuteTemplate(w, v.Layout, vd)
}

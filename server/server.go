package server

import (
  "goji.io"
  "goji.io/pat"
  "bitbucket.org/pathornteng/loomtest/controller"
  "net/http"
)

func Serve() {
  mux := goji.NewMux()
  mc := controller.NewMainController()
  mux.HandleFunc(pat.Get("/register"), mc.RegisterForm)
  mux.HandleFunc(pat.Post("/register"), mc.Register)
  mux.HandleFunc(pat.Get("/user_id/:Id"), mc.UserId)
  mux.HandleFunc(pat.Get("/email/:Email"), mc.Email)
  mux.HandleFunc(pat.Get("/email_improve/:Email"), mc.EmailImprove)
  http.ListenAndServe(":3000", mux)
}

package main

import (
  "fmt"
  "bitbucket.org/pathornteng/loomtest/server"
)

func main() {
  fmt.Println("server is running on port 3000")
  server.Serve()
}

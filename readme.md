# Loom Test

### Project Summary

Loom Test is a repository that contain the source code that support following requirement

1. That takes in a username and an email address, and stores it into a LevelDB database.  Then gives the user back a unique ID for the user. The key to the data should be this unique ID.
2. Create an endpoint that retrieves the user by the uniqueid 
3. Create an endpoint that retrieves the user by email
4. Create your own secondary index on LevelDB to query by email, so you don’t have to scan the entire database

### Installation

```bashp
go get https://bitbucket.org/pathornteng/loomtest
```

### Run

```bashp
cd /your_repository_location
go run main
```

### Usage

```bashp
go to http://localhost:3000/register to register a user
go to http://localhost:3000/user_id/{user_id} to retrive the user info
go to http://localhost:3000/email/{user_email} to retrive the user info (this one will iterate through DB)
go to http://localhost:3000/email_improve/{user_email} to retrive user info (this one will use email as a secondary index to access user info)
```


package controller

import (
  "github.com/syndtr/goleveldb/leveldb"
  "bitbucket.org/pathornteng/loomtest/view"
  "github.com/satori/go.uuid"
  "goji.io/pat"
  "strings"
  "net/http"
  "log"
)

type User struct {
  username      string
  email         string
}

type MainController struct {
  register      *view.View
  registerForm  *view.View
  user          *view.View
  err           *view.View
}

func NewMainController() *MainController {
  mc := MainController{
    register: view.NewView("base","view/register.gohtml"),
    registerForm: view.NewView("base", "view/register_form.gohtml"),
    user: view.NewView("base", "view/user.gohtml"),
    err: view.NewView("base", "view/error.gohtml"),
  }
  return &mc
}

func (MainController *MainController) RegisterForm(w http.ResponseWriter, r *http.Request) {
  MainController.registerForm.Render(w, nil) 
}

func (MainController *MainController) Register(w http.ResponseWriter, r *http.Request) {
  username := strings.Replace(r.FormValue("username"),",","",-1)
  email := strings.Replace(r.FormValue("email"),",","",-1)

  if CheckDup(username, email) {
    MainController.err.Render(w, "Username or Email is duplicated")
    return
  }
  // Need to do data validation / duplication check 

  data := username + "," + email

  u1 := uuid.Must(uuid.NewV4())

  db, err := leveldb.OpenFile("level.db", nil)
	if err != nil {
		log.Fatal("Error", err)
	}
	defer db.Close()

  //main index
  err = db.Put([]byte(u1.String()), []byte(data), nil)

  //secondlevel index
  err = db.Put([]byte(email), []byte(u1.String()), nil)

  MainController.register.Render(w, u1)
}

func (MainController *MainController) UserId(w http.ResponseWriter, r *http.Request) {
  Id := pat.Param(r, "Id")

  db, err := leveldb.OpenFile("level.db", nil)
  if err != nil {
    MainController.err.Render(w, "Cannot open DB")
    log.Fatal("Error", err)
  }
  defer db.Close()

  data, err := db.Get([]byte(Id), nil)
  if err != nil {
    MainController.err.Render(w, "User not found")
    return
  }
  user := strings.Split(string(data), ",")
  MainController.user.Render(w, user)
}

func (MainController *MainController) Email(w http.ResponseWriter, r *http.Request) {
  Email := pat.Param(r, "Email")

  db, err := leveldb.OpenFile("level.db", nil)
  if err != nil {
    MainController.err.Render(w, "Cannot open DB")
    log.Fatal("Error", err)
  }
  defer db.Close()

  found := false
  iter := db.NewIterator(nil, nil)
  for iter.Next() {
    //key := iter.Key()
    value := iter.Value()
    u := strings.Split(string(value), ",")
    if len(u) == 2 {
      if string(u[1]) == Email {
        found = true
        break;
      }
    }
  }
  if !found {
    MainController.err.Render(w, "User not found")
    iter.Release()
    err = iter.Error()
    return 
  }
 
  user := strings.Split(string(iter.Value()), ",")
  iter.Release()
  err = iter.Error()
  MainController.user.Render(w, user)
}

func (MainController *MainController) EmailImprove(w http.ResponseWriter, r *http.Request) {
  Email := pat.Param(r, "Email")

  db, err := leveldb.OpenFile("level.db", nil)
  if err != nil {
    MainController.err.Render(w, "Cannot open DB")
    log.Fatal("Error", err)
  }
  defer db.Close()  

  key, err := db.Get([]byte(Email), nil)
  if err != nil {
    MainController.err.Render(w, "User not found")
    return
  }

  data, err := db.Get(key, nil)
  if err != nil {
    MainController.err.Render(w, "User not found")
    return
  }

  user := strings.Split(string(data), ",")
  MainController.user.Render(w, user)
}

func CheckDup(username string, email string) bool {
  db, err := leveldb.OpenFile("level.db", nil)
  if err != nil {
    log.Fatal("Error", err)
  }
  defer db.Close()

  found := false
  iter := db.NewIterator(nil, nil)
  for iter.Next() {
    //key := iter.Key()
    value := iter.Value()
    u := strings.Split(string(value), ",")
    if len(u) == 2 {
      if string(u[1]) == email {
        found = true
        break;
      }
      if string(u[0]) == username {
        found = true
        break;
      }
    }
  }
  iter.Release()
  err = iter.Error()

  return found
}
